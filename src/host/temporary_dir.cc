#include "host/temporary_dir.h"
#include <string>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace host {

TemporaryDir::TemporaryDir() {
    std::string unique_name = boost::uuids::to_string(boost::uuids::random_generator()());
    path_ = std::filesystem::temp_directory_path() / unique_name;
    std::filesystem::create_directory(path_);
}

const std::filesystem::path& TemporaryDir::path() const {
    return path_;
}

TemporaryDir::~TemporaryDir() {
    std::filesystem::remove_all(path_);
}

}  // namespace host
