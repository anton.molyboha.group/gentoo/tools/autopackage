#ifndef TEMPORARY_DIR_202208160035
#define TEMPORARY_DIR_202208160035

#include <filesystem>

namespace host {

class TemporaryDir {
    std::filesystem::path path_;
public:
    TemporaryDir();
    TemporaryDir(const TemporaryDir&) = delete;
    TemporaryDir(TemporaryDir&&) = delete;
    TemporaryDir& operator=(const TemporaryDir&) = delete;
    const std::filesystem::path& path() const;
    ~TemporaryDir();
};

}  // namespace host

#endif
