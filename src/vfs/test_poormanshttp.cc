#include "vfs/poormanshttp.h"
#include <iostream>

int main(int argc, const char* argv[]) {
    if (argc != 3) {
        std::cerr << "Usage: test_poormanshttp baseurl path" << std::endl;
        return 1;
    }
    vfs::PoorMansHttpFS fs { argv[1] };
    vfs::wrapped_istream strm = fs.read(argv[2]);
    std::string str;
    while (static_cast<std::istream&>(strm)) {
        static_cast<std::istream&>(strm) >> str;
        std::cout << str << std::endl;
    }
    return 0;
}
