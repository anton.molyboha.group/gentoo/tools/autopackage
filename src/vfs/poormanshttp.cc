#include "vfs/poormanshttp.h"

#include <filesystem>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>

namespace vfs {

namespace {
    std::string add_slash_if_necessary(const std::string& url) {
        if (url.empty() || (url.back() != '/')) {
            return url + '/';
        } else {
            return url;
        }
    }

    std::string url_encode_path(const std::filesystem::path& p) {
        std::ostringstream strm;
        strm << std::hex;
        bool need_slash = false;
        for (auto elem : p) {
            if (need_slash) {
                strm << '/';
            }
            need_slash = true;
            for (char c : elem.generic_string()) {
                strm << '%' << static_cast<int>(c / 0x10) << static_cast<int>(c % 0x10);
            }
        }
        return strm.str();
    }
}  // namespace

PoorMansHttpFS::PoorMansHttpFS(const std::string& base_url)
: base_url_(add_slash_if_necessary(base_url)) {}

wrapped_istream PoorMansHttpFS::read(const path& p) {
    const std::filesystem::path rpath = ("/" / p).lexically_normal().relative_path();
    const std::filesystem::path localpath = tmp.path() / rpath;
    if (!std::filesystem::exists(localpath)) {
        std::filesystem::create_directories(localpath.parent_path());
        const std::string full_url = base_url_ + url_encode_path(rpath);
        std::ostringstream command;
        command << "wget -O \"" << localpath << "\" \"" << full_url << "\"";
        const int retcode = std::system(command.str().c_str());
        if (retcode != 0) {
            std::ostringstream msg;
            msg << "Could not download " << full_url;
            throw std::runtime_error(msg.str());
        }
    }
    return wrapped_istream::make<std::ifstream>(localpath);
}

}  // namespace vfs
