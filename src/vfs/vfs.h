#ifndef VFS_H_202208032043
#define VFS_H_202208032043

#include <iostream>
#include <memory>
#include <filesystem>

namespace vfs {
    class wrapped_istream {
        std::shared_ptr<std::istream> base_;
        wrapped_istream(std::shared_ptr<std::istream>&& base) : base_(base) {}
    public:
        template<typename Stream, typename... Args>
        static wrapped_istream make(Args... args) {
            return wrapped_istream(std::make_shared<Stream>(std::forward<Args>(args)...));
        }

        operator std::istream& () {
            return *base_;
        }
    };

//    template<typename Data>
//    auto operator>> (wrapped_istream& strm, Data& data) {
//        return static_cast<std::istream&>(strm) >> data;
//    }

    class wrapped_ostream {
        std::shared_ptr<std::ostream> base_;
        wrapped_ostream(std::shared_ptr<std::ostream>&& base) : base_(base) {}
    public:
        template<typename Stream, typename... Args>
        static wrapped_ostream make(Args... args) {
            return wrapped_ostream(std::make_shared<Stream>(std::forward<Args>(args)...));
        }

        operator std::ostream& () {
            return *base_;
        }
    };

    typedef std::filesystem::path path;

    class VirtualFileSystemRO {
    public:
        virtual wrapped_istream read(const path&) = 0;
        virtual ~VirtualFileSystemRO() {};
    };

    class VirtualFileSystem : public virtual VirtualFileSystemRO {
    public:
        virtual wrapped_ostream write(const path&) = 0;
    };
}  // namespace vfs

#endif
