#include "vfs/subdir.h"
#include <fstream>

namespace vfs {

std::filesystem::path SubdirFileSystemBase::full_path(const path& p) {
    return root_ / ("/" / p).lexically_normal().relative_path();
}

wrapped_istream SubdirFileSystemRO::read(const path& p) {
    return wrapped_istream::make<std::ifstream>(full_path(p));
}

wrapped_ostream SubdirFileSystem::write(const path& p) {
    return wrapped_ostream::make<std::ofstream>(full_path(p));
}

}  // namespace vfs
