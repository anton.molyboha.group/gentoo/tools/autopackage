#ifndef POORMANSHTTP_H_202208160002
#define POORMANSHTTP_H_202208160002

#include "vfs/vfs.h"
#include "host/temporary_dir.h"

namespace vfs {

class PoorMansHttpFS : public VirtualFileSystemRO {
    std::string base_url_;
    host::TemporaryDir tmp;
public:
    PoorMansHttpFS(const std::string& base_url);
    virtual wrapped_istream read(const path&);
};

}  // namespace vfs

#endif
