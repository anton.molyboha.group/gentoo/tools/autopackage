#ifndef SUBDIR_H_202208032043
#define SUBDIR_H_202208032043

#include "vfs/vfs.h"
#include <filesystem>

namespace vfs {

class SubdirFileSystemBase {
protected:
    std::filesystem::path root_;
    std::filesystem::path full_path(const path& p);
public:
    SubdirFileSystemBase(const std::filesystem::path& root) : root_(root) {}
    SubdirFileSystemBase(std::filesystem::path&& root) : root_(root) {}
};

class SubdirFileSystemRO : public virtual VirtualFileSystemRO, virtual SubdirFileSystemBase {
public:
    SubdirFileSystemRO(const std::filesystem::path& root) : SubdirFileSystemBase(root) {}
    SubdirFileSystemRO(std::filesystem::path&& root) : SubdirFileSystemBase(std::move(root)) {}
    virtual wrapped_istream read(const path&);
};

class SubdirFileSystem : public virtual VirtualFileSystem, public virtual SubdirFileSystemRO, virtual SubdirFileSystemBase {
public:
    SubdirFileSystem(const std::filesystem::path& root) : SubdirFileSystemBase(root), SubdirFileSystemRO(root) {}
    SubdirFileSystem(std::filesystem::path&& root) : SubdirFileSystemBase(std::move(root)), SubdirFileSystemRO(root_) {}
    virtual wrapped_ostream write(const path&);
};

}  // namespace vfs

#endif
