#include <iostream>
#include <string>
#include <cstdlib>
#include <optional>
#include <ctime>
#include "go/gosum.h"
#include "vfs/vfs.h"
#include "vfs/subdir.h"
#include "host/temporary_dir.h"

using host::TemporaryDir;

//////////////
// Declarations
//////////////

class DendriteEbuild {
    std::string copyright_header() const;
    std::optional<std::string> commit;
    go::GoSum gosum;
public:
    DendriteEbuild(const go::GoSum gosum, std::optional<std::string_view> commit) : gosum(gosum), commit(commit) {}
    DendriteEbuild(go::GoSum&& gosum) : gosum(gosum) {}
    void write_ebuild(std::ostream& strm) const;
};

void download_source_for_release(const std::filesystem::path& to_dir, const std::string& package_version);

void download_source_for_commit(const std::filesystem::path& to_dir, const std::string& commit);

DendriteEbuild prepare_ebuild(vfs::VirtualFileSystemRO& source);

void write_out_ebuild(const DendriteEbuild& ebuild, const std::string& version, vfs::VirtualFileSystem& repo);

void update_manifest(const std::filesystem::path& ebuild_path);

//////////////
// Definitions
//////////////

std::string DendriteEbuild::copyright_header() const {
    return "# Copyright 2022 Gentoo Authors\n"
           "# Distributed under the terms of the GNU General Public License v2\n";
}

void DendriteEbuild::write_ebuild(std::ostream& strm) const {
    strm << copyright_header();
    strm << '\n';
    strm << "EAPI=7\n"
            "\n"
            "inherit go-module golang-build\n"
            "\n";
    if (commit) {
        strm << "GIT_COMMIT=\"" << *commit << "\"\n";
        strm << "\n";
    }
    strm << "DESCRIPTION=\"Matrix server written in Go\"\n"
            "HOMEPAGE=\"https://matrix.org https://github.com/matrix-org/dendrite\"\n"
            "\n"
            "LICENSE=\"Apache-2.0\"\n"
            "SLOT=\"0\"\n"
            "KEYWORDS=\"~amd64\"\n"
            "IUSE=\"\"\n"
            "\n"
            "DEPEND=\"\"\n"
            "RDEPEND=\"acct-user/dendrite acct-group/dendrite\"\n"
            "BDEPEND=\"\"\n"
            "\n";
    if (commit) {
        strm << "S=\"${WORKDIR}/dendrite-${GIT_COMMIT}\"\n"
             << "\n";
    }
    strm << "EGO_SUM=(\n";
    for (const auto entry : gosum.dependencies()) {
        strm << "\t\"" << entry.dependency.import_path_string() << " " << entry.version << "\"\n";
    }
    strm << ")\n";
    strm << '\n';
    strm << "go-module_set_globals\n"
            "\n";
    strm << "SRC_URI=\"";
    if (commit) {
        strm << "https://github.com/matrix-org/dendrite/archive/${GIT_COMMIT}.tar.gz";
    } else {
        strm << "https://github.com/matrix-org/dendrite/archive/v${PV}.tar.gz";
    }
    strm << " -> ${P}.tar.gz ${EGO_SUM_SRC_URI}\"\n";
    strm << "GO_IMPORTPATH=\"github.com/matrix-org/dendrite\"\n"
            "EGO_PN=\"${GO_IMPORTPATH}/cmd/...\"\n"
            "\n"
            "OUT_GOPATH=\"${S}/go-path\"\n"
            "\n"
            "src_unpack() {\n"
            "\tgo-module_src_unpack || die\n"
            "}\n"
            "\n"
            "src_compile() {\n"
            "\tenv GOPATH=\"${OUT_GOPATH}\":/usr/lib/go-gentoo GOCACHE=\"${T}\"/go-cache CGO_ENABLED=1 \\\n"
            "\t\tgo build -trimpath -v -x -work -o bin/ \"${S}\"/cmd/... || die\n"
            "}\n"
            "\n"
            "src_test() {\n"
            "\tenv GOPATH=\"${OUT_GOPATH}\":/usr/lib/go-gentoo GOCACHE=\"${T}\"/go-cache go test -trimpath -v -x -work \"${S}\"/cmd/... || die\n"
            "}\n"
            "\n"
            "src_install() {\n"
            "\tlocal f\n"
            "\tfor f in $(ls \"${S}/bin/\") ; do\n"
            "\t\tdobin \"${S}/bin/${f}\"\n"
            "\tdone\n"
            "\n"
            "\tdodir \"/etc/dendrite\"\n"
            "\tinsinto /etc/dendrite\n"
            "\tdoins \"${S}/dendrite-sample.monolith.yaml\"\n"
            "\tdoins \"${S}/dendrite-sample.polylith.yaml\"\n"
            "\tnewinitd \"${FILESDIR}\"/dendrite.initd dendrite\n"
            "\tnewconfd \"${FILESDIR}\"/dendrite.confd dendrite\n"
            "\n"
            "\tkeepdir \"/var/log/dendrite\"\n"
            "\tfowners dendrite:dendrite \"/var/log/dendrite\"\n"
            "\n"
            "\teinfo \"If you are upgrading from dendrite 0.5.X or earlier, you may need to update your config to the new format.\"\n"
            "\teinfo \"See ${EPREFIX}/etc/dendrite/dendrite-sample.monolith.yaml or\"\n"
            "\teinfo \"${EPREFIX}/etc/dendrite/dendrite-sample.polylith.yaml for an example.\"\n"
            "}\n";
}

void download_source_for_release(const std::filesystem::path& to_dir, const std::string& package_version) {
    TemporaryDir archive_dir;
    std::string url = std::string("https://github.com/matrix-org/dendrite/archive/v") + package_version + std::string(".tar.gz");
    {
        std::ostringstream command;
        command << "wget " << "-O " << (archive_dir.path() / "source.tgz") << " " << url;
        std::system(command.str().c_str());
    }
    {
        std::ostringstream command;
        command << "tar -x --gunzip --strip-components=1 -f " << (archive_dir.path() / "source.tgz") << " --directory=" << to_dir;
        std::system(command.str().c_str());
    }
}

void download_source_for_commit(const std::filesystem::path& to_dir, std::string_view commit) {
    TemporaryDir archive_dir;
    const std::string url = [&](){
        std::ostringstream builder;
        builder << "https://github.com/matrix-org/dendrite/archive/" << commit << ".tar.gz";
        return builder.str();
    }();
    {
        std::ostringstream command;
        command << "wget " << "-O " << (archive_dir.path() / "source.tgz") << " " << url;
        std::system(command.str().c_str());
    }
    {
        std::ostringstream command;
        command << "tar -x --gunzip --strip-components=1 -f " << (archive_dir.path() / "source.tgz") << " --directory=" << to_dir;
        std::system(command.str().c_str());
    }
}

DendriteEbuild prepare_ebuild(vfs::VirtualFileSystemRO& source, std::optional<std::string_view> commit) {
    go::GoSum gosum = go::GoSum::parse(source.read(vfs::path("go.sum")));
    return DendriteEbuild(gosum, commit);
}

void write_out_ebuild(const DendriteEbuild& ebuild, const std::string& version, vfs::VirtualFileSystem& repo) {
    ebuild.write_ebuild(repo.write(vfs::path("net-matrix") / "dendrite" / (std::string("dendrite-") + version + std::string(".ebuild"))));
}

void update_manifest(const std::filesystem::path& ebuild_path) {
    TemporaryDir dist_dir;
    std::ostringstream command;
    command << "DISTDIR=" << dist_dir.path() << " ebuild " << ebuild_path << " manifest";
    std::system(command.str().c_str());
}

void work(const std::filesystem::path& repo_path, const std::string& package_version, std::optional<std::string_view> commit) {
    TemporaryDir source_dir;
    if (commit) {
        download_source_for_commit(source_dir.path(), *commit);
    } else {
        download_source_for_release(source_dir.path(), package_version);
    }
    DendriteEbuild ebuild = [&]() {
        vfs::SubdirFileSystemRO source_fs {source_dir.path()};
        return prepare_ebuild(source_fs, commit);
    }();
    const std::string ebuild_version = [&](){
        if (commit) {
            std::time_t today_time;
            std::time(&today_time);
            std::tm* today = std::gmtime(&today_time);
            std::ostringstream builder;
            builder << package_version << '.'
                    << std::setw(4) << std::setfill('0') <<  (1900 + today->tm_year)
                    << std::setw(2) << std::setfill('0') << (1 + today->tm_mon)
                    << std::setw(2) << std::setfill('0') << today->tm_mday;
            return builder.str();
        } else {
            return package_version;
        }
    }();
    {
        vfs::SubdirFileSystem repo_fs {repo_path};
        write_out_ebuild(ebuild, ebuild_version, repo_fs);
    }
    update_manifest(repo_path / "net-matrix" / "dendrite" / (std::string("dendrite-") + ebuild_version + std::string(".ebuild")));
}

int main(int argc, char** argv) {
    if (argc < 3 || argc > 4) {
        std::cerr << "Usage: autopackage-dendrite <repo_path> <version> [<commit>]" << std::endl;
        return 1;
    }
    if (argc < 4) {
        work(argv[1], argv[2], std::nullopt);
    } else {
        work(argv[1], argv[2], argv[3]);
    }
    return 0;
}
