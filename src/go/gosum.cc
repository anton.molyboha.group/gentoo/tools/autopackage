#include "go/gosum.h"

#include <iterator>
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include "common.h"

namespace go {

GoSum::ParseException::ParseException(int position):
    // AutopackageException(absl::StrCat("Parse error in go.mod at char ", position)),
    AutopackageException("Parse error in go.sum"),
    position_(position) {}

bool GoSum::Entry::operator==(const Entry& other) const {
    return (dependency == other.dependency) && (version == other.version) && (checksum == other.checksum);
}

namespace {
    namespace qi = boost::spirit::qi;
    namespace ascii = boost::spirit::ascii;

    struct ParsedImportPath {
        std::string value;
        ParsedImportPath() {}
        ParsedImportPath(const std::string& value) : value(value) {}
        ParsedImportPath& operator=(const std::string& other) {
            value = other;
            return *this;
        }
        operator ImportPath() const { return ImportPath{value}; }
    };

    struct ParsedEntry {
        ImportPath dependency;
        std::string version;
        std::string checksum;

        operator GoSum::Entry () const {
            return GoSum::Entry { ImportPath {dependency}, version, checksum };
        };
    };
}  // namespace
}  // namespace go

BOOST_FUSION_ADAPT_STRUCT(
    go::ParsedEntry,
    dependency, version, checksum
);

namespace go {
namespace {
    template<typename Iterator>
    class GoSumGrammar: public qi::grammar<Iterator, std::vector<GoSum::Entry>()> {
        qi::rule<Iterator, std::string()> word;
        qi::rule<Iterator, ParsedImportPath()> import_path;
        qi::rule<Iterator, ParsedEntry()> gosum_entry;
        qi::rule<Iterator, std::vector<ParsedEntry>()> gosum_pre;
        qi::rule<Iterator, std::vector<GoSum::Entry>()> gosum;
    public:
        GoSumGrammar(): GoSumGrammar::base_type(gosum) {
            word %= +(ascii::print - ascii::space - qi::lit('"'));
            import_path %= word;
            gosum_entry %= (import_path >> (+qi::lit(" ")) >> word) >> (+qi::lit(" ")) >> word;
            gosum_pre %= (gosum_entry % (+qi::lit("\n"))) >> (*qi::lit("\n"));
            gosum %= gosum_pre;
        }
    };
}  // namespace

GoSum::GoSum(const std::vector<GoSum::Entry>& dependencies): dependencies_(dependencies) {}

GoSum GoSum::parse(std::istream& gomod) {
    // boost::spirit requires forward iterator, while the
    // iterator wrappers of input streams are "single pass traversal"
    // iterators.
    // TODO: a generic adapter that wraps an input_iterator into a
    // forward_iterator
    std::string input{std::istreambuf_iterator<char>(gomod),
                      std::istreambuf_iterator<char>()};

    // Parse input.
    auto iterator = input.begin();
    GoSumGrammar<decltype(iterator)> parser;
    std::vector<GoSum::Entry> result;
    const bool success = boost::spirit::qi::parse(iterator, input.end(),
        parser, result);

    if (!success) {
        throw ParseException(iterator - input.begin());
    }
    if (iterator != input.end()) {
        throw ParseException(iterator - input.begin());
    }

    return GoSum(result);
}

const std::vector<GoSum::Entry>& GoSum::dependencies() const {
    return dependencies_;
}

}  // namespace go
