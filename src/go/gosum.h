#ifndef GOSUM_H_202207071822
#define GOSUM_H_202207071822

#include <string>
#include <memory>
#include <vector>
#include <iostream>
#include "common.h"
#include "go/go.h"

namespace go {
/// Representation of a go.sum file.
class GoSum {
  public:
    struct Entry {
        ImportPath dependency;
        std::string version;
        std::string checksum;

        bool operator==(const Entry& other) const;
    };
  private:
    std::vector<Entry> dependencies_;
    GoSum(const std::vector<Entry>& dependencies);
  public:
    static GoSum parse(std::istream& gomod);
    const std::vector<Entry>& dependencies() const;

    class ParseException : public AutopackageException {
      private:
        int position_;
      protected:
        ParseException* new_clone() const override {
            return new ParseException(*this);
        }
      public:
        ParseException(int position);
        std::shared_ptr<ParseException> clone() const {
            return std::shared_ptr<ParseException>(new_clone());
        }
        int position() const {
            return position_;
        }
    };
};
}  // namespace go



#endif
