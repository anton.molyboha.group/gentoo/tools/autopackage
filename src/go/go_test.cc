#define BOOST_TEST_MODULE Go
#include <boost/test/included/unit_test.hpp>

#include "go/go.h"
#include "go/gomod.h"
#include "go/gosum.h"
#include <sstream>

BOOST_AUTO_TEST_CASE(parse_go_mod) {
    const std::string gomod_text =
        "module github.com/user/project\n"
        "\n"
        "require (\n"
        "\tgithub.com/pkg/errors v0.9.1\n"
        "\tgolang.org/x/sys v0.0.0-20201119102817-f84b799fce68\n"
        ")\n"
        "\n"
        "go 1.13\n";
    const std::vector<go::ImportPath> expected = {
        go::ImportPath("github.com/pkg/errors"),
        go::ImportPath("golang.org/x/sys")};
    std::istringstream input{gomod_text};
    try {
        const go::GoMod mod = go::GoMod::parse(input);
        BOOST_TEST(mod.dependencies() == expected);
    } catch(go::GoMod::ParseException& exception) {
        std::cerr << "Parse exception at position " << exception.position() << std::endl;
        BOOST_TEST(0);
    }
}

BOOST_AUTO_TEST_CASE(parse_go_mod_with_comments) {
    const std::string gomod_text =
        "module github.com/user/project\n"
        "\n"
        "require (\n"
        "\tgithub.com/pkg/errors v0.9.1  // comment.\n"
        "\tgolang.org/x/sys v0.0.0-20201119102817-f84b799fce68\n"
        ")\n"
        "\n"
        "go 1.13\n";
    const std::vector<go::ImportPath> expected = {
        go::ImportPath("github.com/pkg/errors"),
        go::ImportPath("golang.org/x/sys")};
    std::istringstream input{gomod_text};
    try {
        const go::GoMod mod = go::GoMod::parse(input);
        BOOST_TEST(mod.dependencies() == expected);
    } catch(go::GoMod::ParseException& exception) {
        std::cerr << "Parse exception at position " << exception.position() << std::endl;
        BOOST_TEST(0);
    }
}

BOOST_AUTO_TEST_CASE(fail_to_parse_go_mod_without_module_declaration) {
    const std::string gomod_text =
        // missing module declaration
        "\n"
        "require (\n"
        "\tgithub.com/pkg/errors v0.9.1\n"
        "\tgolang.org/x/sys v0.0.0-20201119102817-f84b799fce68\n"
        ")\n"
        "\n"
        "go 1.13\n";
    std::istringstream input{gomod_text};
    try {
        const go::GoMod mod = go::GoMod::parse(input);
        BOOST_TEST(false);
    } catch(go::GoMod::ParseException& exception) {
        BOOST_TEST(true);
    }
}

BOOST_AUTO_TEST_CASE(fail_to_parse_go_mod_without_go_version) {
    const std::string gomod_text =
        "module github.com/user/package\n"
        "\n"
        "require (\n"
        "\tgithub.com/pkg/errors v0.9.1\n"
        "\tgolang.org/x/sys v0.0.0-20201119102817-f84b799fce68\n"
        ")\n"
        "\n";
        // missing go version
    std::istringstream input{gomod_text};
    try {
        const go::GoMod mod = go::GoMod::parse(input);
        BOOST_TEST(false);
    } catch(go::GoMod::ParseException& exception) {
        BOOST_TEST(true);
    }
}

BOOST_AUTO_TEST_CASE(fail_to_parse_go_mod_without_requirements) {
    const std::string gomod_text =
        "module github.com/user/package\n"
        "\n"
        // missing requirements list.
        "\n"
        "go 1.13\n";
    std::istringstream input{gomod_text};
    try {
        const go::GoMod mod = go::GoMod::parse(input);
        BOOST_TEST(false);
    } catch(go::GoMod::ParseException& exception) {
        BOOST_TEST(true);
    }
}

BOOST_AUTO_TEST_CASE(fail_to_parse_go_mod_if_requirement_version_missing) {
    const std::string gomod_text =
        "module github.com/user/package\n"
        "\n"
        "require (\n"
        "\tgithub.com/pkg/errors\n"  // version missing
        "\tgolang.org/x/sys v0.0.0-20201119102817-f84b799fce68\n"
        ")\n"
        "\n"
        "go 1.13\n";
    std::istringstream input{gomod_text};
    try {
        const go::GoMod mod = go::GoMod::parse(input);
        BOOST_TEST(false);
    } catch(go::GoMod::ParseException& exception) {
        BOOST_TEST(true);
    }
}

BOOST_AUTO_TEST_CASE(parse_go_sum) {
    const std::string gosum_text =
        "cloud.google.com/go v0.26.0/go.mod h1:aQUYkXzVsufM+DwF1aE+0xfcU+56JwCaLick0ClmMTw=\n"
        "cloud.google.com/go v0.31.0/go.mod h1:aQUYkXzVsufM+DwF1aE+0xfcU+56JwCaLick0ClmMTw=\n"
        "cloud.google.com/go v0.34.0/go.mod h1:aQUYkXzVsufM+DwF1aE+0xfcU+56JwCaLick0ClmMTw=\n";
    const std::vector<go::GoSum::Entry> expected = {
        {go::ImportPath("cloud.google.com/go"), "v0.26.0/go.mod", "h1:aQUYkXzVsufM+DwF1aE+0xfcU+56JwCaLick0ClmMTw="},
        {go::ImportPath("cloud.google.com/go"), "v0.31.0/go.mod", "h1:aQUYkXzVsufM+DwF1aE+0xfcU+56JwCaLick0ClmMTw="},
        {go::ImportPath("cloud.google.com/go"), "v0.34.0/go.mod", "h1:aQUYkXzVsufM+DwF1aE+0xfcU+56JwCaLick0ClmMTw="}};
    std::istringstream input{gosum_text};
    try {
        const go::GoSum gosum = go::GoSum::parse(input);
        BOOST_TEST(gosum.dependencies() == expected);
    } catch(go::GoSum::ParseException& exception) {
        std::cerr << "Parse exception at position " << exception.position() << std::endl;
        BOOST_TEST(0);
    }
}
