#include "go/go.h"

#include <iterator>
#include "common.h"

namespace go {

namespace {

template<typename T>
std::ostream& operator << (std::ostream& strm, const std::vector<T>& vec) {
    strm << '(';
    bool need_comma = false;
    for (const T& el : vec) {
        if (need_comma) {
            strm << ", ";
        }
        strm << el;
        need_comma = true;
    }
    strm << ")";
    return strm;
}

}  // namespace

ImportPath::ImportPath(const std::string& path): path_(path) {}

const std::string& ImportPath::import_path_string() const {
    return path_;
}

bool ImportPath::operator== (const ImportPath& other) const {
    return path_ == other.path_;
}

Source::~Source() {}

// dependencies are the portage package names, not Go modules.
void write_ebuild(const std::string& src_url, const ImportPath& module_name, const std::vector<std::string>& dependencies, std::ostream& strm) {
    strm <<
R"ebuild(# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 golang-build
# inherit golang-vcs golang-build

DESCRIPTION=")ebuild" << "go module " << module_name.import_path_string() << R"ebuild("
HOMEPAGE=""
EGIT_REPO_URI=")ebuild" << src_url << R"ebuild("
EGO_PN=")ebuild" << module_name.import_path_string() << R"ebuild("

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=")ebuild" << format_vector(dependencies, "\n\t") << R"ebuild("
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	mkdir -vp "${S}/src/${EGO_PN}" || die
	EGIT_CHECKOUT_DIR="${S}/src/${EGO_PN}" \
		git-r3_src_unpack || die
}
)ebuild";
}

}  // namespace go
