#include "go/gomod.h"

#include <iterator>
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include "common.h"

namespace go {

GoMod::ParseException::ParseException(int position):
    // AutopackageException(absl::StrCat("Parse error in go.mod at char ", position)),
    AutopackageException("Parse error in go.mod"),
    position_(position) {}

namespace {
    class ModuleName {
        std::string name_;
    public:
        ModuleName() {}
        ModuleName(const std::string& name): name_(name) {}
        ModuleName& operator= (const std::string& name) {
            name_ = name;
            return *this;
        }
        ModuleName& operator= (std::string&& name) {
            name_ = std::move(name);
            return *this;
        }
        std::string value() { return name_; }
    };

    struct ParsedGoMod {
        ModuleName module_name;
        std::vector<ImportPath> requirements;
        std::string go_version;
    };
}  // namespace
}  // namespace go

BOOST_FUSION_ADAPT_STRUCT(
    go::ParsedGoMod,
    module_name, requirements, go_version
);

namespace go {
namespace {
    namespace qi = boost::spirit::qi;
    namespace ascii = boost::spirit::ascii;

    template<typename Iterator>
    class GoModGrammar: public qi::grammar<Iterator, ParsedGoMod()> {
        static ImportPath import_path_from_components(const std::vector<std::string>& path_components) {
            std::ostringstream full_path;
            bool need_slash = false;
            for (const std::string& component : path_components) {
                if (need_slash) {
                    full_path << '/';
                }
                full_path << component;
                need_slash = true;
            }
            return ImportPath(full_path.str());
        }

        struct ComponentsToImportPath {
        private:
            ImportPath result;
        public:
            ComponentsToImportPath(const std::vector<std::string>& path_components): result(import_path_from_components(path_components)) {}
            ComponentsToImportPath() {}
            ComponentsToImportPath& operator= (const std::vector<std::string>& path_components) {
                result = import_path_from_components(path_components);
                return *this;
            }
            operator ImportPath() const { return result; }
        };

        struct ComponentsToModuleName {
        private:
            std::string result;
            static std::string convert(const std::vector<std::string>& components) {
                std::ostringstream full_path;
                bool need_slash = false;
                for (const std::string& component : components) {
                    if (need_slash) {
                        full_path << '/';
                    }
                    full_path << component;
                    need_slash = true;
                }
                return full_path.str();
            }
        public:
            ComponentsToModuleName() {}
            ComponentsToModuleName(const std::vector<std::string>& components) : result(convert(components)) {}
            ComponentsToModuleName& operator= (const std::vector<std::string>& components) {
                result = convert(components);
                return *this;
            }
            operator ModuleName() const { return ModuleName(result); }
        };

        qi::rule<Iterator, std::string()> word_without_slash;
        qi::rule<Iterator, std::vector<std::string>()> path_like;
        qi::rule<Iterator> inline_space;
        qi::rule<Iterator, char()> inline_char;
        qi::rule<Iterator> comment;
        qi::rule<Iterator> comments;
        qi::rule<Iterator, std::vector<std::string>()> module_declaration_components;
        qi::rule<Iterator, ComponentsToModuleName()> module_declaration_intermediate;
        qi::rule<Iterator, ModuleName()> module_declaration;
        qi::rule<Iterator, ComponentsToImportPath()> requirement_intermediate;
        qi::rule<Iterator, ImportPath()> requirement;
        qi::rule<Iterator, std::vector<ImportPath>()> requirements;
        qi::rule<Iterator, std::string()> go_version;
        qi::rule<Iterator, ParsedGoMod()> gomod;
    public:
        GoModGrammar(): GoModGrammar::base_type(gomod) {
            word_without_slash %= +(ascii::print - ascii::space - qi::lit('/'));
            path_like = word_without_slash % qi::lit('/');
            inline_space = qi::omit[ascii::space - qi::lit('\n')];
            inline_char = (ascii::char_ - qi::lit('\n'));
            comment = qi::omit[*inline_space >> -( qi::lit("//") >> *inline_char ) >> qi::lit('\n')];
            comments = *comment;
            module_declaration_components = qi::omit[*ascii::space] >> qi::lit("module") >> qi::omit[+ascii::space] >> path_like >> (+comment);
            module_declaration_intermediate %= module_declaration_components;
            module_declaration %= module_declaration_intermediate;
            requirement_intermediate %= ( qi::omit[*ascii::space] >> path_like >> +inline_space >> qi::omit[word_without_slash] >> (+comment) );
            requirement %= requirement_intermediate;
            requirements = qi::omit[*qi::space] >> qi::lit("require (") >> (*requirement) >> qi::lit(')') >> (+comment);
            go_version = qi::omit[*ascii::space] >> qi::lit("go") >> +inline_space >> word_without_slash >> (+comment);
            gomod %= (*comment) >> module_declaration >> requirements >> go_version;
        }
    };
}  // namespace

GoMod::GoMod(const std::vector<ImportPath>& dependencies):
    dependencies_(dependencies) {}

GoMod GoMod::parse(std::istream& gomod) {
    // boost::spirit requires forward iterator, while the
    // iterator wrappers of input streams are "single pass traversal"
    // iterators.
    // TODO: a generic adapter that wraps an input_iterator into a
    // forward_iterator
    std::string input{std::istreambuf_iterator<char>(gomod),
                      std::istreambuf_iterator<char>()};

    // Parse input.
    auto iterator = input.begin();
    GoModGrammar<decltype(iterator)> parser;
    ParsedGoMod result;
    const bool success = boost::spirit::qi::parse(iterator, input.end(),
        parser, result);

    if (!success) {
        throw ParseException(iterator - input.begin());
    }
    if (iterator != input.end()) {
        throw ParseException(iterator - input.begin());
    }

    return GoMod(result.requirements);
}

const std::vector<ImportPath>& GoMod::dependencies() const {
    return dependencies_;
}
}  // namespace go
