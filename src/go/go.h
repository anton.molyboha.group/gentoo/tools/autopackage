#ifndef GO_H_202012151657
#define GO_H_202012151657

#include <string>
#include <memory>
#include <vector>
#include <iostream>
#include "common.h"

namespace go {

class ImportPath {
    std::string path_;
  public:
    ImportPath() {};
    explicit ImportPath(const std::string& path);
    // The importpath as described in `go help importpath`.
    const std::string& import_path_string() const;
    // How to name the Gentoo package that corresponds to this Go package.
    std::string default_package_name() const;

    bool operator== (const ImportPath& other) const;
};

/// A Source gives access to the files in the package,
/// possibly by downloading the source, or possibly through
/// network access to any requested file.
class Source {
  public:
    virtual std::shared_ptr<std::istream> read(const std::string& filename) = 0;
    virtual std::vector<std::string> listdir(const std::string& dirname) = 0;
    virtual ~Source();
};

std::shared_ptr<Source> get_source(const ImportPath& importpath);

void write_ebuild(const std::string& src_url, const ImportPath& module_name, std::ostream& strm);

}  // namespace go

#endif
