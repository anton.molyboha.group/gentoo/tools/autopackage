#ifndef GOMOD_H_202207021532
#define GOMOD_H_202207021532

#include <string>
#include <memory>
#include <vector>
#include <iostream>
#include "common.h"
#include "go/go.h"

namespace go {
/// Representation of a go.mod file.
class GoMod {
    std::vector<ImportPath> dependencies_;
    GoMod(const std::vector<ImportPath>& dependencies);
  public:
    static GoMod parse(std::istream& gomod);
    const std::vector<ImportPath>& dependencies() const;

    class ParseException : public AutopackageException {
      private:
        int position_;
      protected:
        ParseException* new_clone() const override {
            return new ParseException(*this);
        }
      public:
        ParseException(int position);
        std::shared_ptr<ParseException> clone() const {
            return std::shared_ptr<ParseException>(new_clone());
        }
        int position() const {
            return position_;
        }
    };
};
}  // namespace go

#endif
