#ifndef COMMON_H_202012181321
#define COMMON_H_202012181321

#include <exception>

class AutopackageException : public std::exception {
  private:
    std::string description;
  protected:
    virtual AutopackageException* new_clone() const {
        return new AutopackageException();
    }
  public:
    /// Default constructor.
    AutopackageException() = default;
    /// Constructor with description.
    AutopackageException(const char* desc): description(desc) {}
    AutopackageException(const std::string& desc): description(desc) {}
    /// Return the description.
    virtual const char* what() const noexcept override {
        return description.c_str();
    }
    /// Create a copy of the same dynamic type as *this.
    std::shared_ptr<AutopackageException> clone() const {
        return std::shared_ptr<AutopackageException>(new_clone());
    }
    virtual ~AutopackageException() {}
};

template<typename T>
class FormatVector {
  private:
    const std::vector<T>& vector_;
    const std::string& separator_;
  public:
    FormatVector(const std::vector<T>& vector, const std::string& separator):
        vector_(vector), separator_(separator) {}
    void output(std::ostream& strm) const {
        bool need_separator = false;
        for (const std::string& value : vector_) {
            if (need_separator) {
                strm << separator_;
            }
            strm << value;
            need_separator = true;
        }
    }
};

template<typename T>
FormatVector<T> format_vector(const std::vector<T>& vector, const std::string& separator) {
    return FormatVector<T>(vector, separator);
}

template<typename T>
std::ostream& operator<< (std::ostream& strm, const FormatVector<T>& formatter) {
    formatter.output(strm);
    return strm;
};

#endif
